import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms'
import { CustomValidator } from '../../services/custom-validator'
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { FacebookService, InitParams } from 'ngx-facebook';
import { LoginResponse } from 'ngx-facebook';
import { AuthService as SocialLogin } from "angular2-social-login";
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { LoadingUsername } from '../../services/subjects.service';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: String;
  password: String;

  constructor(private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private fb: FacebookService,
    public _auth: SocialLogin,
    private loadingUsername: LoadingUsername) {
    let initParams: InitParams = {
      appId: '511072109241083',
      xfbml: true,
      version: 'v2.8'
    };
    fb.init(initParams);
  }

  ngOnInit() {

  }

  onLoginSumbit() {
    const user =
      {
        email: this.email,
        password: this.password
      }
    this.authService.authenticateUser(user).subscribe(data => {
      console.log(data);
      if (data.success) {
        this.authService.storeUserData(data.token, data.user);
        this.flashMessage.show('You are now logged in', {
          cssClass: 'alert-success',
          timeout: 5000
        });
        this.loadingUsername.loadingName(1)
        this.router.navigate(['dashboard']);

      } else {
        this.flashMessage.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000
        });
        this.router.navigate(['login']);
      }
    });
  }



  signIn(provider) {
    console.log(provider)
    this._auth.login(provider).subscribe(
      (data) => {
        this.authService.gLogin(data)
        this.flashMessage.show('You are now logged in', {
          cssClass: 'alert-success',
          timeout: 5000
        });
        this.loadingUsername.loadingName(1)
        this.router.navigate(['dashboard']);
      }
    )
  }





}

