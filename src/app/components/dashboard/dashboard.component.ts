import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms'
import { NewsService } from '../../services/news.service'
import { AuthService } from '../../services/auth.service'
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  textNews: any;
  textTitle: any;
  addNewsForm: FormGroup;
  username: any;
  dateOfBirth: any;
  token: any;
  fileToUpload: any;
  tag: string;
  optionsModel: number[];
  myOptions: IMultiSelectOption[];


  constructor(private newsService: NewsService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    private authService: AuthService) { }

  ngOnInit() {
    this.myOptions = [
      { id: 1, name: 'Option 1' },
      { id: 2, name: 'Option 2' },
    ];

    this.newsService.showAllTags().subscribe(data => {
      // TODO: заменить на Array.prototype.map
      for (let i = 0; i < data.length; i++) {
        delete data[i].updatedAt;
        delete data[i].createdAt;
        data[i].name = data[i].tag
        delete data[i].tag;
      }

      console.log(data);

      this.myOptions = data;

    }
    );

    this.authService.getProfile().subscribe(profile => {
      this.token = this.authService.loadToken();
      this.username = profile.user.username;
      this.dateOfBirth = profile.user.dateOfBirth;
    },
      err => {
        console.log(err);
        return false;
      })
  }

  onChangeSelect() {
    console.log(this.optionsModel);
  }



  // @ViewChild("fileInput") file1Input;
  @ViewChild("file1Input") file1Input;
  onNewsSumbit() {
    console.log(1231231);
    let fi = this.file1Input.nativeElement;
    if (fi.files && fi.files[0]) {
      let fileToUpload = fi.files[0];
      this.newsService.addNewsWithFile(fileToUpload, this.textNews, this.textTitle, this.optionsModel).subscribe(data => {
        if (data.success) {
          this.flashMessage.show('You just added a news', {
            cssClass: 'alert-success',
            timeout: 5000
          });
          this.router.navigate(['profile']);
        } else {
          this.flashMessage.show(data.msg, {
            cssClass: 'alert-danger',
            timeout: 5000
          });
          this.router.navigate(['login']);
        }
      });

    }
    else {
      this.newsService.addNews(this.textNews, this.textTitle, this.optionsModel).subscribe(data => {
        if (data.success) {
          this.flashMessage.show('You just added a news', {
            cssClass: 'alert-success',
            timeout: 5000
          });
          // this.router.navigate(['profile']);
        } else {
          this.flashMessage.show(data.msg, {
            cssClass: 'alert-danger',
            timeout: 5000
          });
          this.router.navigate(['login']);
        }
      });
    }
  }




  onUserChange() {
    let body =
      {
        token: this.token,
        username: this.username,
        dateOfBirth: this.dateOfBirth
      }
    this.authService.changeUser(body).subscribe(data => {
      if (data.success) {
        this.flashMessage.show('You just change your profile data', {
          cssClass: 'alert-success',
          timeout: 5000
        });
        this.router.navigate(['profile']);
      } else {
        this.flashMessage.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000
        });
        this.router.navigate(['login']);
      }
    });
  }



  @ViewChild("fileInput") fileInput;
  addFile() {
    let fi = this.fileInput.nativeElement;
    if (fi.files && fi.files[0]) {
      let fileToUpload = fi.files[0];
      this.authService.upload(fileToUpload).subscribe(
        data => {
          this.authService.getProfile().subscribe(
            data => {
              console.log(data)
            },
            error => { })
        },
        error => { })
    }
  }

  onTagSumbit() {
    this.newsService.addTag(this.tag).subscribe(data => {
      if (data.success) {
        this.flashMessage.show('You just added a tag', {
          cssClass: 'alert-success',
          timeout: 5000
        });
        this.tag = '';

        delete data.dbRes.updatedAt;
        delete data.dbRes.createdAt;
        data.dbRes.name = data.dbRes.tag
        delete data.dbRes.tag;
        console.log(data.dbRes);

        this.myOptions = this.myOptions.concat(data.dbRes);
        console.log(this.myOptions)


        // this.router.navigate(['profile']);
      } else {
        this.flashMessage.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000
        });
        this.router.navigate(['login']);
      }
    });
  }
}

