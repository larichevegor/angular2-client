import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'
import { NewsComponent } from '../news/news.component';
import { NewsService } from '../../services/news.service';
import { FormsModule } from '@angular/forms';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms'

@Component({
  selector: 'app-userpage',
  templateUrl: './userpage.component.html',
  styleUrls: ['./userpage.component.css']
})
export class UserpageComponent implements OnInit {
  user: Object;
  news: any;
  id: any;
  username: any;
  avatar: any;
  profile: any;
  options: any;
  isUserPage: any = true;

  constructor(private authService: AuthService,
    private router: Router,
    private newsService: NewsService) { }

  ngOnInit() {
    this.authService.getProfile().subscribe(profile => {
      this.user = profile.user;
      this.avatar = (profile.user.avatar);
      this.id = profile.user.id
      this.options =
        {
          isUserPage: this.isUserPage,
          idOfUser: this.id
        }
      this.authService.showUser(this.id).subscribe(profile => {
        this.username = profile[0].username;
        this.profile = profile[0];
      })
      this.newsService.showNewsForUser(this.id).subscribe(news => {
        this.news = news;
        console.log('asdad', this.news);
      },
        err => {
          console.log(err);
          return false;
        })

    },
      err => {
        console.log(err);
        return false;
      })
  }

}
