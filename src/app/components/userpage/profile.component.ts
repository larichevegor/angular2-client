import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import 'rxjs/add/operator/switchMap';
import { AuthService } from '../../services/auth.service'
import { NewsComponent } from '../news/news.component';
import { NewsService } from '../../services/news.service';
import { FormsModule } from '@angular/forms';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms'



@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: Object;
  news: any;
  id: any;
  avatar: any;
  options: any;
  isUserPage: any = true;

  constructor(private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router,
    private newsService: NewsService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.options =
      {
        isUserPage: this.isUserPage,
        idOfUser: this.id
      }
    this.authService.showUser(this.id).subscribe(profile => {
      this.user = profile[0];
      this.avatar = profile[0].avatar;
      this.newsService.showNewsForUser(this.id).subscribe(news => {
        this.news = news;
      },
        err => {
          console.log(err);
          return false;
        })

    },
      err => {
        console.log(err);
        return false;
      })
  }



}
