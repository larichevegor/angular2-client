import { Component, OnInit, Input, ViewContainerRef, Injector } from '@angular/core';
import { NewsService } from '../../services/news.service'
import { AuthService } from '../../services/auth.service'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms'
import { CustomValidator } from '../../services/custom-validator'
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { UserpageComponent } from '../profile/userpage.component';
import { PaginationService } from '../../services/pagination.service';
import { Overlay } from 'ngx-modialog';
import { Modal } from 'ngx-modialog/plugins/bootstrap';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/observable/of';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { News } from './news-model';
import { Subscription } from 'rxjs/Subscription';
import { EditNewsSubject } from '../../services/subjects.service';



@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  News: any;
  @Input() Options: any;
  idOfNews: any;

  idOfAuthor: any
  usernameOfAuthor: any;
  date: any;
  selectedOption: string;
  tags: any;
  searchtext: any = '';
  optionsModel: number[] = [];
  myOptions: IMultiSelectOption[];
  find = true;
  subscription: Subscription;


  ////////////////
  news: Observable<any>;
  private searchTerms = new Subject<string>();

  /////////

  private allItems: any[];
  pager: any = {};
  pagedItems: any[];



  constructor(private authService: AuthService,
    private newsService: NewsService,
    private flashMessage: FlashMessagesService,
    private paginationService: PaginationService,
    private editNewsSubject: EditNewsSubject) {
    this.subscription = this.editNewsSubject.getUpdatedNews().subscribe(data => {
      console.log('subscription')
      // this.getNews()
      this.getNews()
    })
  }

  ngOnInit(): void {
    console.log(this.Options)
    this.getNews();
    this.newsService.showAllTags().subscribe(data => {
      // TODO: заменить на Array.prototype.map
      for (let i = 0; i < data.length; i++) {
        delete data[i].updatedAt;
        delete data[i].createdAt;
        data[i].name = data[i].tag
        delete data[i].tag;
      }

      this.myOptions = data;

    }
    );
    this.authService.getProfile().subscribe(profile => {
      this.idOfAuthor = profile.user.id
      this.authService.showUser(this.idOfAuthor).subscribe(profile => {
        this.usernameOfAuthor = profile[0].username;
      })

    }, err => {
      console.log(err);
      return false;
    })





    ///////////////////////// SEARCHINg/////////////  /
    // 
    // .subscribe(ss => console.log(ss));
    this.searchTerms
      .debounceTime(300)        // wait 300ms after each keystroke before considering the term
      .distinctUntilChanged()   // ignore if next search term is same as previous
      .flatMap(term => {
        return term   // switch to new observable each time the term changes
          // return the http search observable
          ? this.newsService.search(term, this.Options.idOfUser, this.optionsModel)
          // or the observable of empty heroes if there was no search term
          : Observable.of<any[]>([])
      })
      .catch(error => {
        console.log(error);
        return Observable.of<any[]>([]);
      })
      .subscribe((res) => {
        console.log(res)
        if (res.length == 0) {
          return this.find = false
        }
        this.allItems = res;
        this.setPage(1);
        console.log(this.optionsModel)
      })
  }

  getNews() {
    if (this.Options.isUserPage == false) {
      this.newsService.showAllNews().subscribe(news => {
        console.log('getallNews', news)
        this.News = news;
        this.allItems = this.News;
        this.setPage(1);
      },
        err => {
          console.log(err);
          return false;
        })
    }
    if (this.Options.isUserPage == true) {
      this.newsService.showNewsForUser(this.Options.idOfUser).subscribe(news => {
        console.log('NOVIY DANNIY getForUser', news)
        this.News = news;
        this.allItems = this.News;
        this.setPage(1);

      },
        err => {
          console.log(err);
          return false;
        })
    }

  }



  onDeleteNews(idOfNews) {
    console.log(idOfNews)
    this.newsService.deleteNews(idOfNews).subscribe(news => {
      if (news.success == true) {
        console.log(this.News)
        let indexOfElement = this.allItems.findIndex(x => x.id == idOfNews);
        this.allItems.splice(indexOfElement, 1);
        this.setPage(1);
        this.flashMessage.show('Deleted successful', {
          cssClass: 'alert-success',
          timeout: 5000
        });
      }
      else
        this.flashMessage.show('Это не ваша новость', {
          cssClass: 'alert-danger',
          timeout: 5000
        });
    },
      err => {
        console.log(err);
        return false;
      })

  }

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }

    // get pager object from service
    this.pager = this.paginationService.getPager(this.allItems.length, page);

    // get current page of items
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1).reverse();
  }






  onSelectTag(idOfTag) {
    this.optionsModel = idOfTag;
    this.onChangeSelect(this.optionsModel)
  }

  onChangeSelect(optionsModel) {
    if (optionsModel.length == 0 && this.searchtext.trim().length == 0) {
      console.log(true)
      this.allItems = this.News
      this.find = true
      return this.setPage(1);
    }
    else {
      this.newsService.search(this.searchtext, this.Options.idOfUser, optionsModel).subscribe(news => {
        console.log(news)
        if (news.length != 0) {
          this.allItems = news;
          this.find = true
          return this.setPage(1);
        }
        if (news.length == 0) {
          this.allItems = this.News
          this.find = false
          this.flashMessage.show('Undefined', {
            cssClass: 'alert-success',
            timeout: 5000
          });
          return this.setPage(1);
        }
      },
        err => {
          console.log(err);
          return false;
        })
    }
  }

  ////////////////
  search(term: string): void {
    this.searchtext = term
    this.find = true
    if (term.trim() == '' && this.optionsModel.length == 0) {
      this.allItems = this.News;
      return this.setPage(1);
    }
    this.searchTerms.next(term);
  }
  //////////
}





