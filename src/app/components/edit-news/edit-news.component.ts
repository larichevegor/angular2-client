import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';
import { NewsService } from '../../services/news.service'
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router';
import { EditNewsSubject } from '../../services/subjects.service'




@Component({
    selector: 'edit-news',
    templateUrl: './edit-news.component.html'
})
export class DemoModalServiceFromComponent {
    bsModalRef: BsModalRef;
    @Input() news: any;
    tags: any = []
    constructor(private modalService: BsModalService) { }

    public openModalWithComponent() {
        this.tags = []
        console.log(22222, this.news);
        let news = this.news;
        let list = ['Open a modal with component', 'Pass your data', 'Do something else', '...'];
        this.bsModalRef = this.modalService.show(ModalContentComponent);

        this.bsModalRef.content.title = 'Modal with component';
        this.bsModalRef.content.list = list;
        this.bsModalRef.content.news = news;
        for (let i = 0; i < this.news.tags.length; i++) {
            this.tags.push(this.news.tags[i].id);
        }
        this.bsModalRef.content.optionsModel = this.tags
    }
}

/* This is a component which we pass in modal*/

@Component({
    selector: 'modal-content',
    templateUrl: './modal-window.html',
    styleUrls: ['./modal-window.css']
})

export class ModalContentComponent implements OnInit {
    public title: string;
    public list: any[] = [];
    public news: any = {};
    public textTitle: any;
    tag: any;
    optionsModel: number[];
    myOptions: IMultiSelectOption[];

    constructor(private bsModalRef: BsModalRef,
        private newsService: NewsService,
        private authService: AuthService,
        private editNewsSubject: EditNewsSubject
    ) { }


    ngOnInit() {
        this.myOptions = [
            { id: 1, name: 'Option 1' },
            { id: 2, name: 'Option 2' },
        ];

        this.newsService.showAllTags().subscribe(data => {
            // TODO: заменить на Array.prototype.map
            for (let i = 0; i < data.length; i++) {
                delete data[i].updatedAt;
                delete data[i].createdAt;
                data[i].name = data[i].tag
                delete data[i].tag;
            }

            console.log(data);

            this.myOptions = data;

        }
        );
    }


    @ViewChild("file1Input") file1Input;
    onNewsSumbit() {
        console.log(this.news)
        console.log(this.optionsModel)
        let fi = this.file1Input.nativeElement;
        if (fi.files && fi.files[0]) {
            let fileToUpload = fi.files[0];
            this.newsService.changeNewsWithFile(fileToUpload, this.news.id, this.news.newsText, this.news.newsTitle, this.optionsModel).subscribe(data => {
                if (data.success) {
                    console.log('SUCCESSFUL')
                } else {
                    console.log('NOT SUCCESSFUL')
                }
                this.editNewsSubject.editNews(this.news)
            });
            this.bsModalRef.hide()

        }
        else {
            this.newsService.changeNews(this.news.id, this.news.newsText, this.news.newsTitle, this.optionsModel).subscribe(data => {
                if (data.success) {
                    console.log('data success')
                } else {
                    console.log('not okay')
                }
                this.editNewsSubject.editNews(this.news)
            });
            this.bsModalRef.hide()
        }
    }

    onTagSumbit() {
        this.newsService.addTag(this.tag).subscribe(data => {
            if (data.success) {

                this.tag = '';

                delete data.dbRes.updatedAt;
                delete data.dbRes.createdAt;
                data.dbRes.name = data.dbRes.tag
                delete data.dbRes.tag;
                console.log(data.dbRes);

                this.myOptions = this.myOptions.concat(data.dbRes);
                console.log(this.myOptions)


                // this.router.navigate(['profile']);
            } else {
                console.log('bred')
            }
        });
    }


    onChangeSelect() {
        console.log(this.optionsModel);
    }


}

