import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router'
import { NewsComponent } from '../news/news.component';
import { NewsService } from '../../services/news.service';
import { FormsModule } from '@angular/forms';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms'
import { Subscription } from 'rxjs/Subscription';
import { EditNewsSubject } from '../../services/subjects.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  news: any;
  id: any;
  options: any;
  isUserPage: any = false;
  idOfUser: any = undefined;
  subscription: Subscription;

  constructor(private authService: AuthService,
    private router: Router,
    private newsService: NewsService) { }

  ngOnInit() {
    this.options =
      {
        isUserPage: this.isUserPage,
        idOfUser: this.idOfUser
      }
    this.newsService.showAllNews().subscribe(news => {
      this.news = news;
      console.log('all', this.news);
    },
      err => {
        console.log(err);
        return false;
      })

  }

}
