import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { AuthService as SocialLogin } from "angular2-social-login";
import { LoadingUsername } from '../../services/subjects.service';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';




@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  @Input() username: any;
  id: any;

  constructor(private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
    public _auth: SocialLogin,
    private loadingUsername: LoadingUsername) { }

  ngOnInit() {
    this.loadingUsername.loadingName(1)
  }

  onLogoutClick() {
    this.authService.logout();
    this._auth.logout().subscribe(
      (data) => {//return a boolean value.
      }
    )
    this.flashMessage.show('You are logged out', {
      cssClass: 'alert-success',
      timeout: 3000
    });
    this.router.navigate(['/login']);
    return false;
  }
}
