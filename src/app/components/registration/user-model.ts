export class User {
  username: string;
  email: string;
  password: string;
  dateOfBirth: string;
  sex: string;
  constructor({username = undefined ,email= undefined,password= undefined,dateOfBirth= undefined,sex= undefined} = {}) {
    this.username= username;
    this.email= email;
    this.password= password;
    this.dateOfBirth= dateOfBirth;
    this.sex= sex;
  }
}