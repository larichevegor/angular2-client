import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms'
import { CustomValidator } from '../../services/custom-validator'
import { User } from './user-model'
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  form: FormGroup;
  user: User = new User();
  constructor(formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private flashMessage: FlashMessagesService,
  ) {
    this.form = formBuilder.group(
      {
        username: ['', Validators.compose([CustomValidator.usernamecheck])],
        password: ['', Validators.compose([CustomValidator.passwordcheck])],
        email: ['', Validators.compose([CustomValidator.emailcheck])],
        dateOfBirth: [],
        sex: ['', Validators.compose([CustomValidator.sexcheck])],
      }
    )
  }


  formSubmit() {
    this.user = new User(this.form.value);
    this.authService.registerUser(this.user).subscribe(data => {
      if (data.success) {
        this.flashMessage.show('You are now registered and can log in', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/login']);
      } else {
        this.flashMessage.show('Something went wrong', { cssClass: 'alert-danger', timeout: 3000 });
        this.router.navigate(['/register']);
      }
    });

  }



  ngOnInit() {

  }

}
