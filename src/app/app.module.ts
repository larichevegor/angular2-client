import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { FacebookModule } from 'ngx-facebook';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
import { Angular2SocialLoginModule } from "angular2-social-login";



import { MainPageComponent } from './components/main-page/main-page.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { LoginComponent } from './components/login/login.component';
import { UserpageComponent } from './components/profile/userpage.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProfileComponent } from './components/userpage/profile.component';
import { NewsComponent } from './components/news/news.component';
import { ModalContentComponent, DemoModalServiceFromComponent } from './components/edit-news/edit-news.component';

import { AuthService } from './services/auth.service'
import { AuthGuard } from './services/guard/auth.guard';
import { LoginGuard } from './services/guard/login-reg.guard';
import { NewsService } from './services/news.service'
import { PaginationService } from './services/pagination.service'
import { EditNewsSubject } from './services/subjects.service'
import { LoadingUsername } from './services/subjects.service'


let providers = {
  "google": {
    "clientId": "313249044827-s8l2lf0h29rm8700dr9gr39qia08pvn4.apps.googleusercontent.com"
  },
  "linkedin": {
    "clientId": "LINKEDIN_CLIENT_ID"
  },
  "facebook": {
    "clientId": "FACEBOOK_CLIENT_ID",
    "apiVersion": "<version>" //like v2.4 
  }
};




const appRoutes: Routes = [
  { path: 'main', component: MainPageComponent },
  { path: 'profile', component: UserpageComponent, canActivate: [AuthGuard] },
  { path: 'userpage/:id', component: ProfileComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  {
    path: 'register', component: RegistrationComponent, canActivate: [LoginGuard]
  },
  {
    path: '',
    redirectTo: '/main',
    pathMatch: 'full'
  },
  {
    path: 'main/userpage/:id',
    redirectTo: '/userpage/:id',
    pathMatch: 'full'
  },
  {
    path: 'main/profile',
    redirectTo: '/profile',
    pathMatch: 'full'
  },


  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: '**', component: PagenotfoundComponent }

];



@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    UserpageComponent,
    PagenotfoundComponent,
    MainPageComponent,
    DashboardComponent,
    HomeComponent,
    NavbarComponent,
    ProfileComponent,
    NewsComponent,
    DemoModalServiceFromComponent,
    ModalContentComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    FacebookModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    FlashMessagesModule,
    ModalModule.forRoot(),
    MultiselectDropdownModule,
    Angular2SocialLoginModule
  ],
  entryComponents: [ModalContentComponent,],
  providers: [AuthService, EditNewsSubject,LoadingUsername, DemoModalServiceFromComponent, NewsService, LoginGuard, AuthGuard, ModalContentComponent, FlashMessagesModule, PaginationService],
  bootstrap: [AppComponent]
})
export class AppModule { }

Angular2SocialLoginModule.loadProvidersScripts(providers);

