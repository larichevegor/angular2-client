import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';
import { tokenNotExpired } from 'angular2-jwt'
import { AuthService as SocialLogin } from "angular2-social-login";
import { Router } from '@angular/router';




@Injectable()
export class AuthService {
  authToken: any;
  user: any;


  constructor(private http: Http,
    public _auth: SocialLogin,
    private router: Router) {

  }

  // Google.login
  gLogin(data) {
    return new Promise((resolve, reject) => {
      console.log(data.token)
      if (data.token) {
        return this.http.post(`http://localhost:1337/auth/google`, { access_token: data.token })
          .toPromise()
          .then(response => {
            console.log(response.json().token)
            if (response.status == 200) {
              this.storeUserData(response.json().token, response.json().user)
              
            }
            resolve(response.json());
          })
          .catch(() => console.log('error'));
      } else {
        reject();
      }
    });
  }

  ///////////////
  registerUser(user) {
    const body = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.post('http://localhost:1337/users/register', body, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }

  authenticateUser(user) {
    const body = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.post('http://localhost:1337/auth', body, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });
  }

  getProfile() {
    this.loadToken();
    let headers = new Headers({
      'Content-Type': 'application/json;charset=utf-8',
      'Authorization': 'JWT ' + this.authToken
    });
    return this.http.get('http://localhost:1337/users/profile', { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }


  // facebookAuth() {
  //   this.loadToken();
  //   let headers = new Headers({
  //     'Content-Type': 'application/json;charset=utf-8',
  //   });
  //   return this.http.get('http://localhost:1337/users/auth/facebook', { headers: headers })
  //     .map(res => res.json())
  //     .catch((error: any) => { return Observable.throw(error); });

  // }


  storeUserData(token, user) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.removeItem('id_token');
    localStorage.removeItem('user');
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    return this.authToken = token;
  }

  loggedIn() {
    return tokenNotExpired('id_token');
  }

  changeUser(user) {
    var body = JSON.stringify(user);
    console.log('bjj', body);
    let headers = new Headers({ 'Content-Type': 'application/json; charset=utf-8' });
    return this.http.put('http://localhost:1337/users/', body, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });
  }



  showUser(id) {
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.get('http://localhost:1337/users/profile/' + id, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }

  upload(fileToUpload: any): Observable<any> {
    console.log('azaza');
    let form = new FormData();
    form.append('avatar', fileToUpload);
    let headers = new Headers({ 'token': this.authToken });
    let options = new RequestOptions({ headers: headers });
    return this.http.put("http://localhost:1337/users/setimg/", form, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }


}