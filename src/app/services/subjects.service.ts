import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class EditNewsSubject {
    private subject = new Subject<any>();
    private usernamesubject = new Subject<any>();




    editNews(obj: any) {
        this.subject.next(obj);
    }

    clearMessage() {
        this.subject.next();
    }

    getUpdatedNews(): Observable<any> {
        return this.subject.asObservable();
    }





}

@Injectable()
export class LoadingUsername {
    private subject = new Subject<any>();
    private usernamesubject = new Subject<any>();




    loadingName(obj: any) {
        console.log('loadingName');
        this.subject.next(obj);
    }

    clearMessage() {
        this.subject.next();
    }

    getName(): Observable<any> {
        console.log('getName');
        return this.subject.asObservable();
    }

}