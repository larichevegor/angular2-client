import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http'
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { tokenNotExpired } from 'angular2-jwt'
import { AuthService } from './auth.service'
import { RequestOptions } from '@angular/http';
import { News } from '../components/news/news-model';





@Injectable()
export class NewsService {
  constructor(private http: Http,
    private authService: AuthService) { }
  News: any;
  authToken: any;
  newsId: any;
  idOfAuthor: any;


  deleteNews(newsId) {
    const token = this.authService.loadToken();
    var body = {
      token: token,
      newsId: newsId
    };

    const data = JSON.stringify(body);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.delete('http://localhost:1337/news/', new RequestOptions({
      headers: headers,
      body: data
    }))
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }

  showNewsForUser(idOfAuthor) {
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.get('http://localhost:1337/news/author/' + idOfAuthor, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }

  showAllNews() {
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.get('http://localhost:1337/news/', { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }




  // добавление 
  addNewsWithFile(fileToUpload: any, newsText, newsTitle, tags): Observable<any> {
    console.log('azaza');
    this.authToken = this.authService.loadToken();
    let form = new FormData();
    form.append('avatar', fileToUpload);
    form.append('token', this.authToken);
    form.append('newsText', newsText);
    form.append('newsTitle', newsTitle)
    form.append('tags', tags.toString())
    let headers = new Headers({ 'token': this.authToken });
    let options = new RequestOptions({ headers: headers });
    return this.http.post("http://localhost:1337/news/img/", form, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }
  addNews(newsText, newsTitle, tags) {
    this.authToken = this.authService.loadToken();
    var body = {
      token: this.authToken,
      newsText: newsText,
      newsTitle: newsTitle,
      tags: tags.toString()
    };
    const data = JSON.stringify(body);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.post('http://localhost:1337/news/', data, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }

  /////////////////


  //  Редактирование 
  changeNewsWithFile(fileToUpload: any, newsId, newsText, newsTitle, tags): Observable<any> {
    console.log('azaza');
    this.authToken = this.authService.loadToken();
    let form = new FormData();
    form.append('avatar', fileToUpload);
    form.append('token', this.authToken);
    form.append('newsText', newsText);
    form.append('newsTitle', newsTitle)
    form.append('tags', tags.toString())
    let headers = new Headers({ 'token': this.authToken });
    let options = new RequestOptions({ headers: headers });
    return this.http.put('http://localhost:1337/news/img/' + newsId, form, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }
  changeNews(newsId, newsText, newsTitle, tags) {
    this.authToken = this.authService.loadToken();
    var body = {
      token: this.authToken,
      newsText: newsText,
      newsTitle: newsTitle,
      tags: tags.toString(),
    };
    console.log(body, newsId)
    const data = JSON.stringify(body);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.put('http://localhost:1337/news/' + newsId, data, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }
  ////
  showAllTags() {
    console.log(5);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.get('http://localhost:1337/news/tags', { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }

  addTag(tag) {
    // this.authToken = this.authService.loadToken();
    var body = {
      tag: tag
    };
    const data = JSON.stringify(body);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.post('http://localhost:1337/news/tag', data, { headers: headers })
      .map(res => res.json())
      .catch((error: any) => { return Observable.throw(error); });

  }

  // showNewsByTag(idOfTag) {
  //   let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
  //   return this.http.get('http://localhost:1337/news/tag/' + idOfTag, { headers: headers })
  //     .map(res => res.json())
  //     .catch((error: any) => { return Observable.throw(error); });

  // }




  search(term: string, idOfUser, tags): Observable<any[]> {
    console.log('azaza', term, idOfUser, tags)
    console.log(tags.length)
    let headers = new Headers({
      'Content-Type': 'application/json;charset=utf-8',
    })
    let searchstring = 'http://localhost:1337/news/search?'
    if (term != undefined) {
      searchstring += 'text=' + term + '&'
    }
    if (tags.length != 0) {
      headers = new Headers({
        'Content-Type': 'application/json;charset=utf-8',
        'Tags': tags.toString()
      })
    }
    if (idOfUser != undefined) {
      searchstring += 'id=' + idOfUser + '&'
    }
    console.log(searchstring)
    return this.http
      .get(searchstring, { headers: headers })
      .map(res => res.json())





    // /////// На странице юзера ////////////
    // // С поиском без тэгов с юзером
    // if (term != undefined && tags.length == 0 && idOfUser != undefined) {
    //   return this.http
    //     .get(`http://localhost:1337/news/search?text=${term}&id=${idOfUser}`, { headers: headers })
    //     .map(res => res.json())
    // }
    // // С поиском с тэгами, с юзером
    // if (term != undefined && tags.length != 0 && idOfUser != undefined) {
    //   let headers = new Headers({
    //     'Content-Type': 'application/json;charset=utf-8',
    //     'Tags': tags.toString()
    //   })
    //   return this.http
    //     .get(`http://localhost:1337/news/search?text=${term}&id=${idOfUser}`, { headers: headers })
    //     .map(res => res.json())
    // }

    // // Без поиска с тэгами с юзером
    // if (term == undefined && tags.length != 0 && idOfUser != undefined) {
    //   let headers = new Headers({
    //     'Content-Type': 'application/json;charset=utf-8',
    //     'Tags': tags.toString()
    //   })
    //   return this.http
    //     .get(`http://localhost:1337/news/search?id=${idOfUser}`, { headers: headers })
    //     .map(res => res.json())
    // }
    // /////////////////////////////


    // ////////////////// На главной ///////////////


    // // С поиском без тэгов  без юзера
    // if (term != undefined && tags.length == 0 && idOfUser == undefined) {
    //   return this.http
    //     .get(`http://localhost:1337/news/search?text=${term}`, { headers: headers })
    //     .map(res => res.json())
    // }

    // // С поиском с тэгами без юзера
    // if (term != undefined && tags.length != 0 && idOfUser == undefined) {
    //   let headers = new Headers({
    //     'Content-Type': 'application/json;charset=utf-8',
    //     'Tags': tags.toString()
    //   })
    //   return this.http
    //     .get(`http://localhost:1337/news/search?text=${term}`, { headers: headers })
    //     .map(res => res.json())
    // }


    // // Без поиска с тэгами без юзера
    // if (term == undefined && tags.length != 0 && idOfUser == undefined) {
    //   let headers = new Headers({
    //     'Content-Type': 'application/json;charset=utf-8',
    //     'Tags': tags.toString()
    //   })
    //   return this.http
    //     .get(`http://localhost:1337/news/search`, { headers: headers })
    //     .map(res => res.json())
    // }








  }


}








