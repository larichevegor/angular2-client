import { FormControl } from '@angular/forms'

export class CustomValidator {
  static usernamecheck(control: FormControl) {
    let USERNAME_REGEX = /^[A-ZА-ЯЁ\s-]+$/i
    if (USERNAME_REGEX.test(control.value)) {
      return null
    }
    return { usernotvalidate: true };
  }


  static emailcheck(control: FormControl) {
    let EMAIL_REGEX = /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/;

    if (EMAIL_REGEX.test(control.value)) {
      return null
    }
    return { emailnotvalidate: true };
  }

  static passwordcheck(control: FormControl) {
    let PASS_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/;


    if (PASS_REGEX.test(control.value)) {
      return null
    }
    return { passnotvalidate: true };
  }

  static sexcheck(control: FormControl) {

    if (control.value == "Female" || control.value == "Male") {
      return null
    }
    return { sexnotvalidate: true };
  }


  static useruniquecheck(control: FormControl) {
    return new Promise((resolve) => {
      setTimeout(() => {
        if (control.value == "example")
          resolve({ usernotunique: true });
        else
          resolve(null);
      }, 2000);
    })
  }
}
