import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { LoadingUsername } from './services/subjects.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  username: any
  id: any
  subscription: Subscription;

  constructor(private authService: AuthService,
    private loadingUsername: LoadingUsername) {
    this.subscription = this.loadingUsername.getName().subscribe(data => {
      console.log('GETTING NEW USERNAME')
      this.getusername()
    })
  }




  ngOnInit() {
    this.getusername()
  }

  getusername() {
    console.log('getusername')
    this.authService.getProfile().subscribe(profile => {
      console.log('profile', profile)
      this.id = profile.user.id;
      this.authService.showUser(this.id).subscribe(profile => {
        this.username = profile[0].username;
        console.log('username', this.username)

      })
    },
      err => {
        console.log(err);
        return false;
      })
  }

}


